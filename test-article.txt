## Hello world

This is a *test article*. You can write in Markdown Extra[^1] and it **render** in HTML !

This is code :
```
console.log("yes");
```

And here is a [link](/user/testuser) to the profil of the test user.

> This is a quote.
>
> – [David Libeau](https://davidlibeau.fr)


[^1]: Fediblog is using [Parsedown Extra](https://github.com/erusev/parsedown-extra), an extension of [Parsedown](http://parsedown.org/) that adds support for [Markdown Extra](https://michelf.ca/projects/php-markdown/extra/).