<!DOCTYPE html>

<html lang="en">
    <head>
        <?php View::displayStatic("head"); ?>
        <title><?php Server::display("url"); ?></title>
        <link rel="stylesheet" href="/index.css" type="text/css"/>
    </head>
    <body>
        <?php View::displayStatic("nav"); ?>
        <div class="content">
            <header>
                <h1><?php Server::display("title"); ?></h1>
                <p><?php Server::display("subtitle"); ?></p>
            </header>
            <main>
                <h2>All the articles :</h2>
                <ul><?php echo(Content::list("all","Article","article-li-link")); ?></ul>
            </main>
        </div>
        <?php View::displayStatic("foot"); ?>
    </body>
</html>