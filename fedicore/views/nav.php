<nav id="fedinav">
    <ol class="content">
        <li><a href="/" title="Home"><i class="fa fa-home" aria-hidden="true"></i></a></li>
        <li><a href="/about" title="About"><i class="fa fa-question-circle-o" aria-hidden="true"></i></a></li>
    </ol>
</nav>
<div id="fediloader">
    <i class="fa fa-cog fa-spin fa-3x fa-fw"></i>
</div>