<!DOCTYPE html>

<html lang="en">
    <head>
        <?php View::displayStatic("head"); ?>
        <title><?php echo($fediview->get("data")["title"]); ?> - Article - <?php Server::display("url"); ?></title>
        <link rel="stylesheet" href="/article.css" type="text/css"/>
        <link rel="stylesheet" href="/user-card.css" type="text/css"/>
    </head>
    <body>
        <?php View::displayStatic("nav"); ?>
        <div class="content">
            <header>
                <div class="banner" style="background-image:url(<?php echo(Server::get("route/Content/Article")); ?>/<?php $fediview->display("id"); ?>/header.jpg)"></div>
                <h1><?php echo($fediview->get("data")["title"]); ?></h1>
            </header>
            <main>
                <?php View::render("markdown",$fediview); ?>
            </main>
            <footer>
                <p>Article published the <span title="Published date"><?php echo(date_format(new DateTime($fediview->get("data")["publishedDate"]), 'l jS \of F Y \a\t H:i')); ?></span> by <a href="/<?php Server::display("route/User","/"); echo($fediview->get("data")["author"]); ?>" title="Author">@<?php echo($fediview->get("data")["author"]); ?>@<?php Server::display("domain"); ?></a>.</p>
                <div id="author">
                    <?php View::render("user-card",new User($fediview->get("data")["author"])); ?>
                </div>
            </footer>
        </div>
        <?php View::displayStatic("foot"); ?>
    </body>
</html>