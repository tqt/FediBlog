<?php
function autoloader($class) {
    //echo("<p>[Autoload] :".$class."</p>");
    if(file_exists("controllers/".$class.".php")){
        require_once("controllers/".$class.".php");
    }
    require_once("../fedilib/Parsedown.php");
    require_once("../fedilib/ParsedownExtra.php");
    require_once("../fedilib/ParsedownExtraPlugin.php");
    require_once("../fedilib/mime_content_type_modified.php");
}
spl_autoload_register('autoloader');
?>